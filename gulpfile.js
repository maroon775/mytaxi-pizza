var start = new Date().getTime();
var lr    = require('tiny-lr'); // Минивебсервер для livereload
console.log('tiny-lr - ' + (new Date().getTime() - start));

var start = new Date().getTime();
var watch = require('gulp-watch');
console.log('gulp-watch - ' + (new Date().getTime() - start));

var start = new Date().getTime();
var gulp  = require('gulp'); // Сообственно Gulp JS
console.log('gulp - ' + (new Date().getTime() - start));

var start = new Date().getTime();
var path  = require('path'); // Плагин для Stylus
console.log('path - ' + (new Date().getTime() - start));

var start      = new Date().getTime();
var livereload = require('gulp-livereload'); // Livereload для Gulp
console.log('gulp-livereload - ' + (new Date().getTime() - start));

var start  = new Date().getTime();
var rigger = require('gulp-rigger');
console.log('gulp-rigger - ' + (new Date().getTime() - start));

var start   = new Date().getTime();
var changed = require('gulp-changed');
console.log('gulp-changed - ' + (new Date().getTime() - start));

/*
 var start    = new Date().getTime();
 var imagemin = require('gulp-imagemin'); // Минификация изображений
 console.log('gulp-imagemin - ' + (new Date().getTime() - start));
 */

var start  = new Date().getTime();
var concat = require('gulp-concat'); // Склейка файлов
console.log('gulp-concat - ' + (new Date().getTime() - start));

var start   = new Date().getTime();
var plumber = require('gulp-plumber');
console.log('gulp-plumber - ' + (new Date().getTime() - start));

var start   = new Date().getTime();
var connect = require('connect'); // Webserver
console.log('connect - ' + (new Date().getTime() - start));

var start         = new Date().getTime();
var connectStatic = require('serve-static'); // Webserver
console.log('serve-static:' + (new Date().getTime() - start));

var start  = new Date().getTime();
var server = lr();
console.log('server call - ' + (new Date().getTime() - start));

var __path = {
	build: { //Тут мы укажем куда складывать готовые после сборки файлы
		html     : 'build/',
		js       : 'build/js/',
		css      : 'build/css/',
		img      : 'build/i/',
		bg_images: 'build/i/bg/',
		sprite   : 'build/i/bg/',
		vendor   : 'build/vendor/',
		fonts    : 'build/fonts/'
	},
	src  : { //Пути откуда брать исходники
		vendor: 'vendor/**/*.*',
		html  : 'src/*.html', //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .html
		js    : 'src/js/main.js',//В стилях и скриптах нам понадобятся только main файлы
		style : 'src/style/[^_]*.less',
		img   : ['./src/i/**/*.*', '!./src/i/sprite/**/*.*'], //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
		sprite: 'src/i/sprite/**/*.*',
		fonts : 'src/fonts/**/*.*'
	},
	watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
		html  : 'src/**/*.html',
		js    : 'src/js/**/*',
		style : ['src/style/**/*.less', 'src/style/**/*.css', 'src/style/*.less'],
		img   : ['src/i/**/*.*', '!./src/i/sprite/**/*.*'],
		sprite: 'src/i/sprite/**/*.*',
		vendor: 'vendor/**/*.*',
		fonts : 'src/fonts/**/*.*'
	},
	clean: './build'
};

// HTML
gulp.task('html', function ()
{
	gulp.src(__path.src.html) //Выберем файлы по нужному пути
		.pipe(plumber())
		.pipe(rigger()) //Прогоним через rigger
		.pipe(gulp.dest(__path.build.html)) //Выплюнем их в папку build
		.pipe(livereload(server)); //И перезагрузим наш сервер для обновлений
});

// LESS
gulp.task('style', function ()
{
	var start                = new Date().getTime();
	var LessPluginAutoPrefix = require('less-plugin-autoprefix');
	console.log('less-plugin-autoprefix - ' + (new Date().getTime() - start));

	var start      = new Date().getTime();
	var autoprefix = new LessPluginAutoPrefix({browsers: ["> 1%", "last 2 versions", "Firefox ESR", "Opera 12.1"]});
	console.log('new LessPluginAutoPrefix call - ' + (new Date().getTime() - start));

	var start          = new Date().getTime();
	LessPluginCleanCSS = require('less-plugin-clean-css');
	console.log('less-plugin-clean-css - ' + (new Date().getTime() - start));

	var start = new Date().getTime();
	cleancss = new LessPluginCleanCSS({advanced: true}),
		console.log('new LessPluginCleanCSS call - ' + (new Date().getTime() - start));

	var start = new Date().getTime();
	var less  = require('gulp-less'); // Плагин для Less
	console.log('gulp-less - ' + (new Date().getTime() - start));

	console.log('less begin');
	return gulp.src(__path.src.style)
		.pipe(plumber())
		.pipe(less({
			paths  : [path.join(__dirname, 'less'), path.join(__dirname, 'css')],
			plugins: [cleancss, autoprefix]
		}))
		.pipe(gulp.dest(__path.build.css)) //И в build
		.pipe(livereload(server)); // даем команду на перезагрузку css
});

// JAVASCRIPT
gulp.task('js', function ()
{
	return;

	var start  = new Date().getTime();
	var uglify = require('gulp-uglify'); // Минификация JS
	console.log('gulp-uglify - ' + (new Date().getTime() - start));

	gulp.src(__path.src.js) //Найдем наш main файл
		.pipe(plumber())
		.pipe(rigger()) //Прогоним через rigger
		//.pipe(uglify()) //Сожмем наш js
		.pipe(gulp.dest(__path.build.js)) //Выплюнем готовый файл в build
		.pipe(livereload(server)); // даем команду на перезагрузку страницы
});

// IMAGES MIN
gulp.task('images', function ()
{
	return gulp.src(__path.src.img) //Выберем наши картинки
		.pipe(plumber())
		.pipe(changed(__path.build.img))
		.pipe(gulp.dest(__path.build.img)) //И бросим в build
		.pipe(livereload(server));
	;
});

// FONTS
gulp.task('fonts', function ()
{
	gulp.src(__path.src.fonts)
		.pipe(gulp.dest(__path.build.fonts))
});

// VENDOR PLUGINS
gulp.task('vendor', function ()
{
	return;
	gulp.src(__path.src.vendor)
		.pipe(gulp.dest(__path.build.vendor))
});

// HTTP SERVER
gulp.task('http-server', function ()
{
	connect()
		.use(require('connect-livereload')())
		.use(connectStatic('./build'))
		.listen('9000');
	console.log('Server listening on http://localhost:9000');
});

// BOWER COMPONENTS
gulp.task('bower', function ()
{
	return;
	var start  = new Date().getTime();
	var uglify = require('gulp-uglify'); // Минификация JS
	console.log('gulp-uglify - ' + (new Date().getTime() - start));

	var arJs = [
		'./bower_components/jquery/dist/jquery.min.js',
		'./bower_components/bootstrap/dist/js/bootstrap.min.js',
		'./bower_components/raty/lib/jquery.raty.js',
	];

	gulp.src(arJs)
		.pipe(uglify())
		.pipe(gulp.dest(__path.build.js));

	gulp.src('./bower_components/bootstrap/fonts/**/*.*')
		.pipe(gulp.dest(__path.build.fonts + 'bootstrap/'));

	return;
});

gulp.task('sprite', function ()
{

	var start       = new Date().getTime();
	var spritesmith = require('gulp.spritesmith');
	console.log('gulp.spritesmith - ' + (new Date().getTime() - start));

	var start    = new Date().getTime();
	var pngquant = require('imagemin-pngquant');
	console.log('imagemin-pngquant:' + (new Date().getTime() - start));

	var spriteData =
		    gulp.src(__path.src.sprite) // путь, откуда берем картинки для спрайта
			    .pipe(spritesmith({
				    imgName    : 'sprite.png',
				    cssName    : 'sprite.less',
				    algorithm  : 'top-down',//'binary-tree',
				    imgPath    : '../i/bg/sprite.png',
				    cssTemplate: 'less.template.spritesmith',
				    cssVarMap  : function (sprite)
				    {
					    sprite.name = 'icon--' + sprite.name
				    }
			    }));

	spriteData.img.pipe(gulp.dest(__path.build.sprite)); // путь, куда сохраняем картинку
	spriteData.css.pipe(gulp.dest('./src/style/lib/')); // путь, куда сохраняем стили
});

gulp.task('watch', function ()
{
	// Подключаем Livereload
	livereload.listen(35729, function (err)
	{
		if (err) return console.log(err);
		gulp.watch([__path.watch.html], function (event, cb)
		{
			gulp.start('html');
			console.log('html:change', event)
		});
		gulp.watch([__path.watch.sprite], function (event, cb)
		{
			gulp.start('sprite');
			console.log('sprite:change', event)
		});
		gulp.watch([__path.watch.style], function (event, cb)
		{
			gulp.start('style');
			console.log('style:change', event)
		});
		gulp.watch([__path.watch.js], function (event, cb)
		{
			gulp.start('js');
			console.log('js:change', event)
		});
		gulp.watch(__path.watch.img, function (event, cb)
		{
			gulp.start('images');
			console.log('images:change', event)
		});

		gulp.watch([__path.watch.fonts], function (event, cb)
		{
			gulp.start('fonts');
			console.log('fonts:change', event)
		});
		gulp.watch([__path.watch.vendor], function (event, cb)
		{
			gulp.start('vendor');
			console.log('vendor:change', event)
		});
	});
});

gulp.task('build', [
	'sprite',
	'html',
	'js',
	'style',
	'fonts',
	'images',
	'bower'
]);

gulp.task('clean', function (cb)
{
	var start  = new Date().getTime();
	var rimraf = require('rimraf');
	console.log('rimraf - ' + (new Date().getTime() - start));

	rimraf(__path.clean, cb);
});

gulp.task('zip', function ()
{
	var start = new Date().getTime();
	var zip   = require('gulp-zip');
	console.log('gulp-zip - ' + (new Date().getTime() - start));

	return gulp.src('./build/**/*')
		.pipe(zip('build-' + Math.round((new Date() / 1000)) + '.zip', {compress: false}))
		.pipe(gulp.dest('./dist/'));
});

gulp.task('default', ['build', 'watch', 'http-server']);
gulp.task('chrome', ['build', 'watch', 'http-server'], function ()
{
	var cp = require('child_process');

	cp.exec('"C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe" http://localhost:9000', function ()
	{
		console.log('Runing in browser');
	});
});
